#!/bin/sh
echo Content-type: text/html
echo 
echo 

cat << EOM
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'>
<head><meta http-equiv='Pragma' CONTENT='no-cache' /> <meta http-equiv='content-type' content='text/html; charset=utf-8' /> <title>C6x Linux Web Control Panel</title><link rel='stylesheet' href='/default.css' type='text/css' />
</head>
<body>
<div id='wrapper'><div id='logo'><h1>Flash Read</h1>
</div><div id='header'><div id='menu'><ul>
</ul></div></div></div>
<div id='page'>
EOM

display_footer()
{
cat << EOM2
<hr>
<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2013 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
EOM2
}

TMPFILE=`mktemp`
cat > $TMPFILE

get_params()
{
partition=$(cat $TMPFILE | sed -n '4p'| sed 's/.$//')
mtddevname=$(cat /proc/mtd | awk 'NR < 4' | awk -v pat="$partition" '$0 ~ pat {printf $1}' | sed 's/://')
mtddev="/dev/"$mtddevname
TMPFILE_DATA="../"$partition".bin"
}

check_params()
{

#check for the NAND partition
if [ "$mtddevname" = "" ] ; then
cat << EOM1
<div id='content'>
<p>Could not find Nand partition for <i>$partition</i> ...</p>
<br>
</div>
EOM1
display_footer
rm -f $TMPFILE
exit
fi

mtdsz="0x"$(cat /proc/mtd | awk 'NR < 4' | awk -v pat="$partition" '$0 ~ pat {printf $2}')
mtdsz=$(($mtdsz))
}

read_flash()
{
cat << EOM1
<div id='content'>
<p>Reading <i>$mtdsz</i> bytes from <i>$mtddev</i> ...</p>
</div>
EOM1
nanddump -q -f $TMPFILE_DATA -l $mtdsz $mtddev
sync
}

#Allow User to download the file
file_download()
{
cat << EOM1
<div id='content'>
<input type="button" name="Button" value="Save NAND data" onClick="window.open('$TMPFILE_DATA', 'download'); return false;">
</div>
EOM1
}

get_params
check_params
read_flash
file_download

#cleanup
rm -f $TMPFILE

display_footer

