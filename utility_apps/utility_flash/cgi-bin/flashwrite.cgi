#!/bin/sh
echo Content-type: text/html
echo 
echo 

cat << EOM
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'>
<head><meta http-equiv='Pragma' CONTENT='no-cache' /> <meta http-equiv='content-type' content='text/html; charset=utf-8' /> <title>C6x Linux Web Control Panel</title><link rel='stylesheet' href='/default.css' type='text/css' />
</head>
<body>
<div id='wrapper'><div id='logo'><h1>Flash Write</h1>
</div><div id='header'><div id='menu'><ul>
</ul></div></div></div>
<div id='page'>
EOM

display_footer()
{
cat << EOM2
<hr>
<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2013 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
EOM2
}
TMPFILE=`mktemp`
TMPFILE_DATA=`mktemp`
cat > $TMPFILE
partition=$(cat $TMPFILE | sed -n '4p'| sed 's/.$//')
mtddevname=$(cat /proc/mtd | awk 'NR < 4' | awk -v pat="$partition" '$0 ~ pat {printf $1}' | sed 's/://')
mtddev="/dev/"$mtddevname
soffset=$(head -8 $TMPFILE | wc -c)
eoffset=$(tail -1 $TMPFILE | wc -c)
fsize=$(cat $TMPFILE | wc -c)
osize=$(((($fsize-$eoffset)-$soffset)-2))

check_params()
{
#check for 0 size file
if [ $osize -eq 0 ] ; then
cat << EOM1
<div id='content'>
<p>Bad Input file ...</p>
</div>
EOM1
display_footer
rm -f $TMPFILE
exit
fi

#check for the NAND partition
if [ "$mtddevname" = "" ] ; then
cat << EOM1
<div id='content'>
<p>Could not find Nand partition for <i>$partition</i> ...</p>
<br>
</div>
EOM1
display_footer
rm -f $TMPFILE
exit
fi

mtdsz="0x"$(cat /proc/mtd | awk 'NR < 4' | awk -v pat="$partition" '$0 ~ pat {printf $2}')
mtdsz=$(($mtdsz))
if [ $osize -gt $mtdsz ] ; then
cat << EOM1
<div id='content'>
<p>Input File too large (File Size=$osize, MTD Size=$mtdsz)</p>
</div>
EOM1
display_footer
rm -f $TMPFILE
exit
fi
}

erase_flash()
{
cat << EOM1
<div id='content'>
<p>Erasing mtd device <i>$mtddev</i> ...</p>
<br>
</div>
EOM1
flash_eraseall -q $mtddev
}

program_flash()
{
dd if=$TMPFILE of=$TMPFILE_DATA skip=$soffset bs=1 count=$osize 2> /dev/null
cat << EOM1
<div id='content'>
<p>Writing <i>$osize</i> bytes to <i>$mtddev</i> ...</p>
</div>
EOM1
nandwrite -q $mtddev $TMPFILE_DATA -p
sync
cat << EOM2
<div id='content'>
<p>Write to <i>$mtddev</i> complete</p>
</div>
EOM2
}

check_params
erase_flash
program_flash

#cleanup
rm -f $TMPFILE
rm -f $TMPFILE_DATA

display_footer

