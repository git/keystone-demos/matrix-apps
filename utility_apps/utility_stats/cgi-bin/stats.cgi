#!/bin/sh

echo Content-type: text/html
echo 
echo 
cat << EOM
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Keystone II Utility Application System Statistics</title>
<META HTTP-EQUIV="REFRESH" CONTENT="60">
</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>System Statistics</h1></div>
<div id='header'>
<div id='menu'>
</div>

<div id='page'>
<div id='content'>
<hr><table cellpadding='6'><tr>
<td valign='center'><image src='apps/images/utilities-c66x.png'></td>
<td width='100%' align='justify'>
<h2>Statistics</h2><br>
<p class='Description'><b>Description:</b> This page displays system statistics like Memory and CPU usage. This page refreshes itself every 60 seconds.</p>
</td></tr></table><hr><br>
</div>
EOM

TMPFILE=`mktemp`

cat << TOPINFO1  
<div>
<h3>Memory & CPU Usage</h3>
<PRE>
TOPINFO1

top -bn1 > $TMPFILE
sed '1,10!d' $TMPFILE

cat << TOPINFO2 
</PRE>
</div><br>
TOPINFO2

cat << EOF
<hr>
<div style="clear: both;">&nbsp;</div>
</div>
<div id="footer">
	<p id="legal">( c ) 2013 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</div>
</body>
</html>
EOF

