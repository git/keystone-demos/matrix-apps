#!/bin/sh
echo Content-type: text/html
echo 
echo 

display_footer()
{
cat << EOM2
<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2013 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
EOM2
}

cat << EOM
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'>
<head><meta http-equiv='Pragma' CONTENT='no-cache' /> <meta http-equiv='content-type' content='text/html; charset=utf-8' /> <title>C6x Linux Web Control Panel</title>
</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>EEPROM Read</h1></div>
<div id='header'>
<div id='menu'>
</div>
</div>
</div>
<div id='page'>
EOM
TMPFILE=`mktemp`
cat > $TMPFILE

get_params()
{
busAddr=$(cat $TMPFILE | sed -n '4p' | sed 's/.$//')
eepromdev=$(find /sys -name eeprom)
readSz=65536
echo $busAddr
echo $eepromdev

TMPFILE_DATA="../i2c_"$busAddr".bin"
if [ "$busAddr" = "0x51" ] ; then
devOffset=$readSz
else
devOffset=0
fi
echo $devOffset


}

check_params()
{
echo
}

#Read EEPROM 
read_eeprom()
{
cat << EOM1
<div id='content'>
<p>Reading <i>$readSz</i> bytes from EEPROM Bus Address <i>$busAddr</i> ...</p>
</div>
EOM1
dd of=$TMPFILE_DATA if=$eepromdev bs=1 count=$readSz skip=$devOffset 2> /dev/null
sync
}

#Allow User to download the file
file_download()
{
cat << EOM1
<div id='content'>
<input type="button" name="Button" value="Save EEPROM data" onClick="window.open('$TMPFILE_DATA', 'download'); return false;">
</div>
EOM1
}

get_params
check_params
read_eeprom
file_download

display_footer
rm -f $TMPFILE
#rm -f $TMPFILE_DATA


