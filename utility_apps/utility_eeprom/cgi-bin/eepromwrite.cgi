#!/bin/sh
echo Content-type: text/html
echo 
echo 

display_footer()
{
cat << EOM2
<div style='clear: both;'>&nbsp;</div>
</div>
<div id='footer'><p id='legal'>( c ) 2013 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></div>
</body>
</html>
EOM2
}

cat << EOM
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'>
<head><meta http-equiv='Pragma' CONTENT='no-cache' /> <meta http-equiv='content-type' content='text/html; charset=utf-8' /> <title>C6x Linux Web Control Panel</title>
</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>EEPROM Write</h1></div>
<div id='header'>
<div id='menu'>
</div>
</div>
</div>
<div id='page'>
EOM
TMPFILE=`mktemp`
cat > $TMPFILE
get_params()
{
busAddr=$(cat $TMPFILE | sed -n '4p' | sed 's/.$//')
eepromdev=$(find /sys -name eeprom)
soffset=$(head -8 $TMPFILE | wc -c)
eoffset=$(tail -1 $TMPFILE | wc -c)
fsize=$(cat $TMPFILE | wc -c)
osize=$(((($fsize-$eoffset)-$soffset)-2))
eepromSz=65536
if [ "$busAddr" = "0x51" ] ; then
devOffset=$eepromSz
else
devOffset=0
fi
}

# write eeprom
write_eeprom()
{
wOffset=$devOffset
cat << EOM1
<div id='content'>
<p>Writing EEPROM <i>$osize</i> bytes to <i>$eepromdev</i> ...</p>
<p>This step would take few minutes ...</p>
</div>
EOM1
dd if=$TMPFILE of=$eepromdev bs=1 count=$osize seek=$wOffset skip=$soffset 2> /dev/null
sync
rm -f $TMPFILE
}

check_params()
{
if [ $osize -eq 0 ] ; then
cat << EOM1
<div id='content'>
<p>Bad Input file ...</p>
</div>
EOM1
display_footer
rm -f $TMPFILE
exit
fi
if [ $osize -gt $eepromSz ] ; then
cat << EOM1
<div id='content'>
<p>Input file Size greater than <i>$eepromSz</i> bytes ...</p>
</div>
EOM1
display_footer
rm -f $TMPFILE
exit
fi
}

get_params
check_params
write_eeprom

cat << EOM2
<div id='content'>
<p>EEPROM programming complete</p>
</div>
EOM2

display_footer

