#!/bin/sh

echo Content-type: text/html
echo 
echo 
cat << EOM
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Keystone II Utility Application System Infomation</title>
</head>
<body>
<div id='wrapper'>
<div id='logo'><h1>Information</h1></div>
<div id='header'>
<div id='menu'>
</div>

<div id='page'>
<div id='content'>
<hr>
<table cellpadding='6'>
<tr>
<td valign='center'><image src='apps/images/utilities-c66x.png'></td>
<td width='100%' align='justify'>
<h2>Information</h2><br>
<p class='Description'><b>Description:</b> This page displays the static system information. </p>
</td>
</tr>
</table>
<hr>
</div>
<br>
EOM
	
cat << UPT1
<div>
<h3>System Up Time</h3>	
<PRE>
UPT1

cat /proc/uptime

cat << UPT2
seconds
</PRE>
</div><br>
UPT2

cat << CPUINFO1 
<div>
<h3>CPU Info</h3>
<PRE>
CPUINFO1

cat /proc/cpuinfo

cat << CPUINFO2 
</PRE>
</div><br>
CPUINFO2

cat << SWINFO1 
<div>
<h3>Linux version</h3>
<PRE>
SWINFO1

uname -srm

cat << SWINFO2 
</PRE>
</div><br>
SWINFO2

TMPFILE=`mktemp`
mount | awk ' {print "device ", $1," mounted on ",$3, " as ", $5, "filesystem"}' > $TMPFILE

cat << MOUNTINFO1  
<div>
<h3>Mount Info</h3>
<PRE>
MOUNTINFO1

cat $TMPFILE
rm $TMPFILE

cat << MOUNTINFO2 
</PRE>
</div><br>
MOUNTINFO2

cat << IFCONFIG1  
<div>
<h3>Network Interfaces</h3>
<PRE>
IFCONFIG1

ifconfig

cat << IFCONFIG2 
</PRE>
</div><br>
IFCONFIG2

cat << ROUTEINFO1  
<div>
<h3>Routing table</h3>
<PRE>
ROUTEINFO1

route

cat << ROUTEINFO2 
</PRE>
</div><br>
ROUTEINFO2


cat << MODINFO1  
<div>
<h3>Module Info</h3>
<PRE>
MODINFO1

lsmod

cat << MODINFO2 
</PRE>
</div><br>
MODINFO2

cat << EOF
<hr>
<div style='clear: both;'>&nbsp;</div>
</div>
<div id="footer">
	<p id="legal">( c ) 2013 Texas Instruments Incorporated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</div>
</body>
</html>
EOF

