#!/bin/sh
# Module: utilities_main
#
# Description: This script is used to run the utilities application demo
# 
# Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#
#  Redistribution and use in source and binary forms, with or withou
#  modification, are permitted provided that the following conditions
#  are met:
#
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#  
#  Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the
#  distribution.
#
#  Neither the name of Texas Instruments Incorporated nor the names of
#  its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# Check platform type based on device tree compatible entry
compatible=$(cat /proc/device-tree/compatible)
case "$compatible" in
  *k2hk-evm*) 
	platform=k2hk-evm
	num_dsps=8
	dsp_image=messageq_single.xe66
	;;
  *k2l-evm*)  
	platform=k2l-evm
	num_dsps=4
	dsp_image=messageq_single.k2l.xe66
	;;
  *k2e-evm*)  
	platform=k2e-evm
	num_dsps=1
	dsp_image=messageq_single.k2e.xe66
	;;
  *)	
	platform=unknown
	num_dsps=0
	;;
esac
echo platform : $platform num_dsps:  $num_dsps

max_dsp_num=$((num_dsps-1))
echo Loading DSP Images
for i in `seq 0 $max_dsp_num`
do
echo "Resetting core $i..."
mpmcl reset dsp$i
mpmcl status dsp$i
done
echo "Done"
echo "Loading and Running " $1 "..."
for i in `seq 0 $max_dsp_num`
do
echo "/lib/firmware/$dsp_image"
mpmcl load dsp$i "/lib/firmware/$dsp_image"
mpmcl run dsp$i
done

echo Running MessageQBench:
/usr/bin/MessageQBench
