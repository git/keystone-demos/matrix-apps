#!/bin/sh

export SLAVE_DIR=/usr/share/matrix-gui-2.0/apps/demo_imageproc/bin

compatible=$(cat /proc/device-tree/compatible)
case "$compatible" in
  *k2hk-evm*) 
	platform=k2hk-evm
	num_dsps=8
	dsp_image=image_processing_evmtci6638k2k_slave.out
	;;
  *k2l-evm*)  
	platform=k2l-evm
	num_dsps=4
	dsp_image=image_processing_evmtci6630k2l_slave.out
	;;
  *k2e-evm*)  
	platform=k2e-evm
	num_dsps=1
	dsp_image=image_processing_evm66ak2e_slave.out
	;;
  *)	
	platform=unknown
	num_dsps=0
	;;
esac
echo platform : $platform num_dsps:  $num_dsps
max_dsp_num=$((num_dsps-1))

for i in `seq 0 $max_dsp_num`
do
mpmcl reset dsp$i
mpmcl load dsp$i $SLAVE_DIR/${dsp_image}
mpmcl run dsp$i
done
